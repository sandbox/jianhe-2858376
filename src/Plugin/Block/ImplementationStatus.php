<?php

namespace Drupal\bs_maintenance\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block to display implementation status.
 *
 * @Block(
 *   id="maintenance_implementation_status",
 *   admin_label = @Translation("Maintenance implementation status")
 * )
 */
class ImplementationStatus extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#markup' => $this->t('Please check the <a href=":url">Maintenance Management Implementation Status</a> and finish the system implementation.', [
        ':url' => Url::fromRoute('maintenance.implementation_status')->toString(),
      ])
    ];
  }

}
