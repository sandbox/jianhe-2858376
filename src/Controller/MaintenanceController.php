<?php

namespace Drupal\bs_maintenance\Controller;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;

/**
 * Returns response for Maintenance routes.
 */
class MaintenanceController extends ControllerBase {

  public function implementationStatus() {
    $items = [];

    $items[] = $this->checkEntity(count($items) + 1,
      $this->t('Create an Organization'), 'organization');
    $items[] = $this->checkInventoryOrganization(count($items) + 1,
      $this->t('Set Up an Inventory Organization'));
    $items[] = $this->checkEntity(count($items) + 1,
      $this->t('Set Up Items'), 'item');
    $items[] = $this->checkEntity(count($items) + 1,
      $this->t('Set Up Subinventory'), 'subinventory');
    $items[] = $this->checkEntity(count($items) + 1,
      $this->t('Set Up Master Configuration'), 'master_configuration');
    $items[] = [
      '#markup' => $this->t('Step @step <a href=":url">Remove the implementation status block</a>', [
        '@step' => count($items) + 1,
        ':url' => Url::fromRoute('block.admin_display')->toString(),
      ]),
      '#wrapper_attributes' => [
        'class' => [
          'not-finished',
        ],
      ],
    ];

    $output = [
      '#theme' => 'item_list',
      '#wrapper_attributes' => [
        'class' => [
          'implementation-status',
        ],
      ],
      '#items' => $items,
      '#attached' => [
        'library' => [
          'bs_maintenance/implementation_status',
        ],
      ],
    ];

    $tags = $this->entityTypeManager()->getDefinition('organization')
      ->getListCacheTags();
    $tags = Cache::mergeTags($tags,
      $this->entityTypeManager->getDefinition('item')
        ->getListCacheTags());
    $tags = Cache::mergeTags($tags,
      $this->entityTypeManager->getDefinition('subinventory')
        ->getListCacheTags());
    $tags = Cache::mergeTags($tags,
      $this->entityTypeManager->getDefinition('master_configuration')
        ->getListCacheTags());
    $output['#cache']['tags'] = $tags;

    return $output;
  }

  protected function checkEntity($step, $title, $entity_type_id) {
    $count = $this->entityTypeManager->getStorage($entity_type_id)->getQuery()
      ->count()
      ->execute();
    if ($count) {
      $item = t('Step @step <a href=":url">@title</a>', [
        '@step' => $step,
        ':url' => Url::fromRoute('entity.' . $entity_type_id . '.collection')->toString(),
        '@title' => $title,
      ]);
    }
    else {
      $text = t('Step @step <a href=":url">@title</a> (Not Finished)', [
        '@step' => $step,
        ':url' => Url::fromRoute('entity.' . $entity_type_id . '.collection')->toString(),
        '@title' => $title,
      ]);
      $item = [
        '#markup' => $text,
        '#wrapper_attributes' => [
          'class' => [
            'not-finished',
          ],
        ],
      ];
    }
    return $item;
  }

  protected function checkInventoryOrganization($step, $title) {
    $count = $this->entityTypeManager->getStorage('organization')->getQuery()
      ->condition('inventory_organization', TRUE)
      ->count()
      ->execute();
    if ($count) {
      $item = t('Step @step <a href=":url">@title</a>', [
        '@step' => $step,
        ':url' => Url::fromRoute('entity.organization.collection')->toString(),
        '@title' => $title,
      ]);
    }
    else {
      $text = t('Step @step <a href=":url">@title</a> (Not Finished)', [
        '@step' => $step,
        ':url' => Url::fromRoute('entity.organization.collection')->toString(),
        '@title' => $title,
      ]);
      $item = [
        '#markup' => $text,
        '#wrapper_attributes' => [
          'class' => [
            'not-finished',
          ],
        ],
      ];
    }
    return $item;
  }

}
