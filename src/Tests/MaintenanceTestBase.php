<?php

namespace Drupal\bs_maintenance\Tests;

use Drupal\cbo_item\Tests\ItemTestBase;

/**
 * Provides tool functions.
 */
abstract class MaintenanceTestBase extends ItemTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['bs_maintenance'];

}
