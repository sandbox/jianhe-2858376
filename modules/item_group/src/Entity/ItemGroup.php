<?php

namespace Drupal\item_group\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\item_group\ItemGroupInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Defines the item group entity class.
 *
 * @ContentEntityType(
 *   id = "item_group",
 *   label = @Translation("Item Group"),
 *   handlers = {
 *     "access" = "Drupal\item_group\ItemGroupAccessControlHandler",
 *     "views_data" = "Drupal\item_group\ItemGroupViewsData",
 *     "form" = {
 *       "default" = "Drupal\item_group\ItemGroupForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "list_builder" = "Drupal\Core\Entity\EntityListBuilder",
 *   },
 *   base_table = "item_group",
 *   entity_keys = {
 *     "id" = "gid",
 *     "label" = "title",
 *     "uuid" = "uuid",
 *   },
 *   admin_permission = "administer item groups",
 *   links = {
 *     "add-form" = "/admin/item_group/add",
 *     "canonical" = "/admin/item_group/{item_group}",
 *     "edit-form" = "/admin/item_group/{item_group}/edit",
 *     "delete-form" = "/admin/item_group/{item_group}/delete",
 *     "collection" = "/admin/item_group",
 *   },
 * )
 */
class ItemGroup extends ContentEntityBase implements ItemGroupInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['number'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Number'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'type' => 'string',
        'weight' => -6,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -6,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['status'] = BaseFieldDefinition::create('state')
      ->setLabel(t('Status'))
      ->setRequired(TRUE)
      ->setSetting('workflow', 'item_group_status')
      ->setDefaultValue('draft')
      ->setDisplayOptions('view', [
        'type' => 'list_default',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['item_list'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Item List'))
      ->setSetting('target_type', 'item_group_item')
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_label',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'inline_entity_form_complex',
        'weight' => 0,
        'settings' => [
          'form_mode' => 'default',
          'allow_new' => TRUE,
          'allow_existing' => TRUE,
          'match_operator' => 'CONTAINS',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The timestamp that the item_group was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The timestamp that the item_group was last changed.'));

    return $fields;
  }

}
