<?php

namespace Drupal\item_group\Tests;

use Drupal\item_group\Entity\Component;
use Drupal\cbo_item\Tests\ItemTestBase;
use Drupal\item_group\Entity\ItemGroup;

/**
 * Provides tool functions.
 */
abstract class ItemGroupTestBase extends ItemTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['item_group'];

  /**
   * A item group.
   *
   * @var \Drupal\item_group\ItemGroupInterface
   */
  protected $item_group;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->item_group = $this->createItemGroup();
  }

  /**
   * Creates a item_group based on default settings.
   */
  protected function createItemGroup(array $settings = []) {
    // Populate defaults array.
    $settings += [
      'number' => $this->randomMachineName(8),
      'title' => $this->randomMachineName(8),
    ];
    $entity = ItemGroup::create($settings);
    $entity->save();

    return $entity;
  }

}
