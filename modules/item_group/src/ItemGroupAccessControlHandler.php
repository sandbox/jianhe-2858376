<?php

namespace Drupal\item_group;

use Drupal\Core\Entity\EntityAccessControlHandler;

/**
 * Defines the access control handler for the item_group entity type.
 *
 * @see \Drupal\item_group\Entity\ItemGroup
 */
class ItemGroupAccessControlHandler extends EntityAccessControlHandler {

}
