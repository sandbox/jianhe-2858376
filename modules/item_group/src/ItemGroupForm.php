<?php

namespace Drupal\item_group;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form handler for the item_group edit forms.
 */
class ItemGroupForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $item_group = $this->entity;
    $insert = $item_group->isNew();
    $item_group->save();
    $item_group_link = $item_group->link($this->t('View'));
    $context = array('%title' => $item_group->label(), 'link' => $item_group_link);
    $t_args = array('%title' => $item_group->link($item_group->label()));

    if ($insert) {
      $this->logger('item_group')->notice('ItemGroup: added %title.', $context);
      drupal_set_message(t('ItemGroup %title has been created.', $t_args));
    }
    else {
      $this->logger('item_group')->notice('ItemGroup: updated %title.', $context);
      drupal_set_message(t('ItemGroup %title has been updated.', $t_args));
    }
  }

}
