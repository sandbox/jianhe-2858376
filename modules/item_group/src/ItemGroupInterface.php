<?php

namespace Drupal\item_group;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a item group entity.
 */
interface ItemGroupInterface extends ContentEntityInterface {

}
