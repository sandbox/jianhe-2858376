<?php

namespace Drupal\item_group;

use Drupal\views\EntityViewsData;

/**
 * Provides the views data for the item_group entity types.
 */
class ItemGroupViewsData extends EntityViewsData {

}
