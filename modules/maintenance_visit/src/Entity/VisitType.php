<?php

namespace Drupal\maintenance_visit\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\maintenance_visit\VisitTypeInterface;

/**
 * Defines the Visit type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "visit_type",
 *   label = @Translation("Visit type"),
 *   handlers = {
 *     "access" = "Drupal\maintenance_visit\VisitTypeAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\maintenance_visit\VisitTypeForm",
 *       "delete" = "Drupal\maintenance_visit\Form\VisitTypeDeleteConfirm"
 *     },
 *     "list_builder" = "Drupal\maintenance_visit\VisitTypeListBuilder",
 *   },
 *   admin_permission = "administer maintenance_visit types",
 *   config_prefix = "type",
 *   bundle_of = "maintenance_visit",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   links = {
 *     "edit-form" = "/admin/maintenance_visit/type/{visit_type}",
 *     "delete-form" = "/admin/maintenance_visit/type/{visit_type}/delete",
 *     "collection" = "/admin/maintenance_visit/type",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *   }
 * )
 */
class VisitType extends ConfigEntityBundleBase implements VisitTypeInterface {

  /**
   * The machine name of this Visit type.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the Visit type.
   *
   * @var string
   */
  protected $label;

  /**
   * A brief description of this Visit type.
   *
   * @var string
   */
  protected $description;

  /**
   * {@inheritdoc}
   */
  public function isLocked() {
    $locked = \Drupal::state()->get('maintenance_visit.type.locked');
    return isset($locked[$this->id()]) ? $locked[$this->id()] : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

}
