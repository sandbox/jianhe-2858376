<?php

namespace Drupal\maintenance_visit\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\maintenance_visit\MaintenanceVisitInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Defines the maintenance_visit entity class.
 *
 * @ContentEntityType(
 *   id = "maintenance_visit",
 *   label = @Translation("Maintenance Visit"),
 *   handlers = {
 *     "storage" = "Drupal\maintenance_visit\MaintenanceVisitStorage",
 *     "view_builder" = "Drupal\maintenance_visit\MaintenanceVisitViewBuilder",
 *     "access" = "Drupal\maintenance_visit\MaintenanceVisitAccessControlHandler",
 *     "views_data" = "Drupal\maintenance_visit\MaintenanceVisitViewsData",
 *     "form" = {
 *       "default" = "Drupal\maintenance_visit\MaintenanceVisitForm",
 *       "delete" = "Drupal\maintenance_visit\Form\MaintenanceVisitDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "list_builder" = "Drupal\Core\Entity\EntityListBuilder",
 *   },
 *   base_table = "maintenance_visit",
 *   entity_keys = {
 *     "id" = "bid",
 *     "label" = "title",
 *     "uuid" = "uuid",
 *   },
 *   admin_permission = "administer maintenance_visits",
 *   links = {
 *     "add-form" = "/admin/maintenance_visit/add",
 *     "canonical" = "/admin/maintenance_visit/{maintenance_visit}",
 *     "edit-form" = "/admin/maintenance_visit/{maintenance_visit}/edit",
 *     "delete-form" = "/admin/maintenance_visit/{maintenance_visit}/delete",
 *     "collection" = "/admin/maintenance_visit",
 *   },
 * )
 */
class MaintenanceVisit extends ContentEntityBase implements MaintenanceVisitInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['organization'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Organization'))
      ->setSetting('target_type', 'organization')
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_label',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => -3,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['item'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Parent item'))
      ->setRequired(TRUE)
      ->setSetting('target_type', 'item')
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_label',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => -3,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['description'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Description'))
      ->setDisplayOptions('view', [
        'type' => 'string',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
        'weight' => 0,
        'settings' => [
          'rows' => 4,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['attachments'] = BaseFieldDefinition::create('file')
      ->setLabel(t('Attachments'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('view', [
        'type' => 'file_default',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'file_generic',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The timestamp that the maintenance_visit was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The timestamp that the maintenance_visit was last changed.'));

    return $fields;
  }

}
