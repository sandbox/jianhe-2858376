<?php

namespace Drupal\master_configuration\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\master_configuration\MasterConfigurationInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Defines the master configuration entity class.
 *
 * @ContentEntityType(
 *   id = "master_configuration",
 *   label = @Translation("Master Configuration"),
 *   handlers = {
 *     "view_builder" = "Drupal\master_configuration\MasterConfigurationViewBuilder",
 *     "access" = "Drupal\master_configuration\MasterConfigurationAccessControlHandler",
 *     "views_data" = "Drupal\master_configuration\MasterConfigurationViewsData",
 *     "form" = {
 *       "default" = "Drupal\master_configuration\MasterConfigurationForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "list_builder" = "Drupal\master_configuration\MasterConfigurationListBuilder",
 *   },
 *   base_table = "master_configuration",
 *   entity_keys = {
 *     "id" = "rid",
 *     "label" = "title",
 *     "uuid" = "uuid",
 *   },
 *   admin_permission = "administer master configurations",
 *   links = {
 *     "add-form" = "/admin/master_configuration/add",
 *     "canonical" = "/admin/master_configuration/{master_configuration}",
 *     "edit-form" = "/admin/master_configuration/{master_configuration}/edit",
 *     "delete-form" = "/admin/master_configuration/{master_configuration}/delete",
 *     "collection" = "/admin/master_configuration",
 *   },
 * )
 */
class MasterConfiguration extends ContentEntityBase implements MasterConfigurationInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['number'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Number'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'type' => 'string',
        'weight' => -6,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -6,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['position'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Category'))
      ->setSetting('target_type', 'configuration_position')
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_label',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'inline_entity_form_complex',
        'weight' => 0,
        'settings' => [
          'form_mode' => 'default',
          'allow_new' => TRUE,
          'allow_existing' => FALSE,
          'match_operator' => 'CONTAINS',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['status'] = BaseFieldDefinition::create('state')
      ->setLabel(t('Status'))
      ->setRequired(TRUE)
      ->setSetting('workflow', 'master_configuration_status')
      ->setDefaultValue('draft')
      ->setDisplayOptions('view', [
        'type' => 'list_default',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The timestamp that the master_configuration was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The timestamp that the master_configuration was last changed.'));

    return $fields;
  }

}
