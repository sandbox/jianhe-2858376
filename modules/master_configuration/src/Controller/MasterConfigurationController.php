<?php

namespace Drupal\master_configuration\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\master_configuration\MasterConfigurationInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Returns responses for MasterConfiguration routes.
 */
class MasterConfigurationController extends ControllerBase {

  public function applyTransition(Request $request, MasterConfigurationInterface $master_configuration, $transition_id) {
    /** @var \Drupal\state_machine\Plugin\Field\FieldType\StateItem $state_item */
    $state_item = $master_configuration->get('status')->first();
    $transitions = $state_item->getTransitions();
    if ($transition = $transitions[$transition_id]) {
      $state_item->applyTransition($transition);
      $master_configuration->save();
    }

    if ($destination = $request->query->get('destination')) {
      return new RedirectResponse($destination);
    }
    else {
      return new RedirectResponse($master_configuration->toUrl()->toString());
    }
  }

}
