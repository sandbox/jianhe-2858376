<?php

namespace Drupal\master_configuration;

use Drupal\views\EntityViewsData;

/**
 * Provides the views data for the configuration_position entity types.
 */
class ConfigurationPositionViewsData extends EntityViewsData {

}
