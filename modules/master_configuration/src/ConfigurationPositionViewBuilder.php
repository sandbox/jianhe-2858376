<?php

namespace Drupal\master_configuration;

use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\Core\Url;

/**
 * View builder handler for Configuration positions.
 */
class ConfigurationPositionViewBuilder extends EntityViewBuilder {

}
