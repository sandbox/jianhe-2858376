<?php

namespace Drupal\master_configuration;

use Drupal\views\EntityViewsData;

/**
 * Provides the views data for the master_configuration entity types.
 */
class MasterConfigurationViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['master_configuration']['bulk_form'] = [
      'title' => $this->t('Operations bulk form'),
      'help' => $this->t('Add a form element that lets you run operations on multiple master configurations.'),
      'field' => [
        'id' => 'bulk_form',
      ],
    ];

    return $data;
  }

}
