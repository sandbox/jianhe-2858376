<?php

namespace Drupal\master_configuration;

use Drupal\Core\Entity\EntityAccessControlHandler;

/**
 * Defines the access control handler for the master_configuration entity type.
 *
 * @see \Drupal\master_configuration\Entity\MasterConfiguration
 */
class MasterConfigurationAccessControlHandler extends EntityAccessControlHandler {

}
