<?php

namespace Drupal\master_configuration;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a configuration position entity.
 */
interface ConfigurationPositionInterface extends ContentEntityInterface {

}
