<?php

namespace Drupal\master_configuration;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form handler for the configuration_position edit forms.
 */
class ConfigurationPositionForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $master_configuration = $this->entity;
    $insert = $master_configuration->isNew();
    $master_configuration->save();
    $master_configuration_link = $master_configuration->link($this->t('View'));
    $context = array('%title' => $master_configuration->label(), 'link' => $master_configuration_link);
    $t_args = array('%title' => $master_configuration->link($master_configuration->label()));

    if ($insert) {
      $this->logger('master_configuration')->notice('ConfigurationPosition: added %title.', $context);
      drupal_set_message(t('ConfigurationPosition %title has been created.', $t_args));
    }
    else {
      $this->logger('master_configuration')->notice('ConfigurationPosition: updated %title.', $context);
      drupal_set_message(t('ConfigurationPosition %title has been updated.', $t_args));
    }
  }

}
