<?php

namespace Drupal\master_configuration;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the configuration_position entity type.
 *
 * @see \Drupal\master_configuration\Entity\ConfigurationPosition
 */
class ConfigurationPositionAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  public function access(EntityInterface $entity, $operation, AccountInterface $account = NULL, $return_as_object = FALSE) {
    switch ($operation) {
      case 'view':
      case 'update':
        /** @var \Drupal\master_configuration\MasterConfigurationInterface $master_configuration */
        if ($master_configuration = $entity->get('master_configuration')->entity) {
          return $master_configuration->access($operation, $account, $return_as_object);
        }
        break;
    }

    return parent::access($entity, $operation, $account, $return_as_object);
  }

}
