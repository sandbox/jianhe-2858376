<?php

namespace Drupal\master_configuration\Tests;

use Drupal\item_group\Tests\ItemGroupTestBase;
use Drupal\master_configuration\Entity\ConfigurationPosition;
use Drupal\master_configuration\Entity\MasterConfiguration;

/**
 * Provides tool functions.
 */
abstract class MasterConfigurationTestBase extends ItemGroupTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['master_configuration'];

  /**
   * A master_configuration.
   *
   * @var \Drupal\master_configuration\MasterConfigurationInterface
   */
  protected $masterConfiguration;

  /**
   * A configuration point.
   *
   * @var \Drupal\master_configuration\ConfigurationPositionInterface
   */
  protected $configurationPosition;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->masterConfiguration = $this->createMasterConfiguration();
    $this->configurationPosition = $this->createConfigurationPosition();
  }

  /**
   * Creates a master_configuration based on default settings.
   */
  protected function createMasterConfiguration(array $settings = []) {
    // Populate defaults array.
    $settings += [
      'number' => $this->randomMachineName(8),
      'title' => $this->randomMachineName(8),
    ];
    $entity = MasterConfiguration::create($settings);
    $entity->save();

    return $entity;
  }

  /**
   * Create a master_configuration component based on default settings.
   */
  protected function createConfigurationPosition(array $settings = []) {
    // Populate defaults array.
    $settings += [
      'number' => $this->randomMachineName(8),
      'title' => $this->randomMachineName(8),
      'master_configuration' => $this->masterConfiguration->id(),
    ];
    $entity = ConfigurationPosition::create($settings);
    $entity->save();

    return $entity;
  }

}
