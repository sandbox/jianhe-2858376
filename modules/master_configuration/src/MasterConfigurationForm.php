<?php

namespace Drupal\master_configuration;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form handler for the master_configuration edit forms.
 */
class MasterConfigurationForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $master_configuration = $this->entity;
    $insert = $master_configuration->isNew();
    $master_configuration->save();
    $master_configuration_link = $master_configuration->link($this->t('View'));
    $context = array('%title' => $master_configuration->label(), 'link' => $master_configuration_link);
    $t_args = array('%title' => $master_configuration->link($master_configuration->label()));

    if ($insert) {
      $this->logger('master_configuration')->notice('MasterConfiguration: added %title.', $context);
      drupal_set_message(t('MasterConfiguration %title has been created.', $t_args));
    }
    else {
      $this->logger('master_configuration')->notice('MasterConfiguration: updated %title.', $context);
      drupal_set_message(t('MasterConfiguration %title has been updated.', $t_args));
    }
  }

}
