<?php

namespace Drupal\master_configuration;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a master configuration entity.
 */
interface MasterConfigurationInterface extends ContentEntityInterface {

}
