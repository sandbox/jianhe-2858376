<?php

namespace Drupal\cbo_maintenance;

use Drupal\Core\Entity\EntityAccessControlHandler;

/**
 * Defines the access control handler for the maintenance_requirement entity type.
 *
 * @see \Drupal\cbo_maintenance\Entity\MaintenanceRequirement
 */
class MaintenanceRequirementAccessControlHandler extends EntityAccessControlHandler {

}
