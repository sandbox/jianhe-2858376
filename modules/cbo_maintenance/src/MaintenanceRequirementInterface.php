<?php

namespace Drupal\cbo_maintenance;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a maintenance requirement entity.
 */
interface MaintenanceRequirementInterface extends ContentEntityInterface {

}
