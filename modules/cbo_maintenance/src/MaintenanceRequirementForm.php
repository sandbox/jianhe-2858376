<?php

namespace Drupal\cbo_maintenance;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form handler for the maintenance_requirement edit forms.
 */
class MaintenanceRequirementForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $insert = $entity->isNew();
    $entity->save();
    $entity_link = $entity->link($this->t('View'));
    $context = array('%title' => $entity->label(), 'link' => $entity_link);
    $t_args = array('%title' => $entity->link($entity->label()));

    if ($insert) {
      $this->logger('cbo_maintenance')->notice('MaintenanceRequirement: added %title.', $context);
      drupal_set_message(t('MaintenanceRequirement %title has been created.', $t_args));
    }
    else {
      $this->logger('cbo_maintenance')->notice('MaintenanceRequirement: updated %title.', $context);
      drupal_set_message(t('MaintenanceRequirement %title has been updated.', $t_args));
    }
  }

}
