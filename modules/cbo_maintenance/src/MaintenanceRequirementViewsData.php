<?php

namespace Drupal\cbo_maintenance;

use Drupal\views\EntityViewsData;

/**
 * Provides the views data for the maintenance_requirement entity types.
 */
class MaintenanceRequirementViewsData extends EntityViewsData {

}
