<?php

namespace Drupal\cbo_maintenance\Tests;

/**
 * Tests maintenance_requirement entities.
 *
 * @group cbo_maintenance
 */
class MaintenanceRequirementTest extends MaintenanceTestBase {

  /**
   * A user with project admin permission.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * Modules to install.
   *
   * @var array
   */
  public static $modules = ['block', 'views'];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->adminUser = $this->drupalCreateUser([
      'administer maintenance requirements',
      'access maintenance requirement',
    ]);
  }

  /**
   * Tests the list, add, save.
   */
  public function testList() {
    $this->drupalPlaceBlock('local_tasks_block');
    $this->drupalPlaceBlock('local_actions_block');

    $this->drupalLogin($this->adminUser);

    $this->drupalGet('admin/maintenance_requirement');
    $this->assertResponse(200);
    $this->assertLinkByHref('admin/maintenance_requirement/add');

    $this->clickLink(t('Add maintenance requirement'));
    $this->assertResponse(200);

    $edit = [
      'title[0][value]' => $this->randomMachineName(8),
      'number[0][value]' => $this->randomMachineName(8),
      'organization[0][target_id]' => $this->organization->label() . ' (' . $this->organization->id() . ')',
    ];
    $this->drupalPostForm(NULL, $edit, t('Save'));
    $this->assertResponse(200);
    $this->assertText($edit['title[0][value]']);
  }

  /**
   * Tests the edit form.
   */
  public function testEdit() {
    $this->drupalPlaceBlock('local_tasks_block');

    $this->drupalLogin($this->adminUser);

    $this->drupalGet('admin/maintenance_requirement/' . $this->maintenanceRequirement->id());
    $this->assertResponse(200);
    $this->assertLinkByHref('admin/maintenance_requirement/' . $this->maintenanceRequirement->id() . '/edit');

    $this->clickLink(t('Edit'));
    $this->assertResponse(200);

    $edit = [
      'title[0][value]' => $this->randomMachineName(8),
    ];
    $this->drupalPostForm(NULL, $edit, t('Save'));
    $this->assertResponse(200);
    $this->assertText($edit['title[0][value]']);
  }

}
