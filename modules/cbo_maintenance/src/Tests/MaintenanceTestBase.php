<?php

namespace Drupal\cbo_maintenance\Tests;

use Drupal\cbo_maintenance\Entity\MaintenanceRequirement;
use Drupal\cbo_organization\Tests\OrganizationTestBase;

/**
 * Provides tool functions.
 */
abstract class MaintenanceTestBase extends OrganizationTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['cbo_maintenance'];

  /**
   * A maintenance requirement.
   *
   * @var \Drupal\cbo_maintenance\MaintenanceRequirementInterface
   */
  protected $maintenanceRequirement;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->maintenanceRequirement = $this->createMaintenanceRequirement();
  }

  /**
   * Creates a maintenance requirement based on default settings.
   */
  protected function createMaintenanceRequirement(array $settings = []) {
    // Populate defaults array.
    $settings += [
      'number' => $this->randomMachineName(8),
      'title' => $this->randomMachineName(8),
      'organization' => $this->organization->id(),
    ];
    $entity = MaintenanceRequirement::create($settings);
    $entity->save();

    return $entity;
  }

}
