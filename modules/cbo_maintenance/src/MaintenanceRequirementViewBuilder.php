<?php

namespace Drupal\cbo_maintenance;

use Drupal\Core\Entity\EntityViewBuilder;

/**
 * View builder handler for BOMs.
 */
class MaintenanceRequirementViewBuilder extends EntityViewBuilder {

}
