<?php

namespace Drupal\unit_configuration\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\unit_configuration\UnitConfigurationInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Defines the unit_configuration entity class.
 *
 * @ContentEntityType(
 *   id = "unit_configuration",
 *   label = @Translation("UnitConfiguration"),
 *   handlers = {
 *     "storage" = "Drupal\unit_configuration\UnitConfigurationStorage",
 *     "view_builder" = "Drupal\unit_configuration\UnitConfigurationViewBuilder",
 *     "access" = "Drupal\unit_configuration\UnitConfigurationAccessControlHandler",
 *     "views_data" = "Drupal\unit_configuration\UnitConfigurationViewsData",
 *     "form" = {
 *       "default" = "Drupal\unit_configuration\UnitConfigurationForm",
 *       "delete" = "Drupal\unit_configuration\Form\UnitConfigurationDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "list_builder" = "Drupal\Core\Entity\EntityListBuilder",
 *   },
 *   base_table = "unit_configuration",
 *   entity_keys = {
 *     "id" = "bid",
 *     "label" = "title",
 *     "uuid" = "uuid",
 *   },
 *   admin_permission = "administer unit_configurations",
 *   links = {
 *     "add-form" = "/admin/unit_configuration/add",
 *     "canonical" = "/admin/unit_configuration/{unit_configuration}",
 *     "edit-form" = "/admin/unit_configuration/{unit_configuration}/edit",
 *     "delete-form" = "/admin/unit_configuration/{unit_configuration}/delete",
 *     "collection" = "/admin/unit_configuration",
 *   },
 * )
 */
class UnitConfiguration extends ContentEntityBase implements UnitConfigurationInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['master_configuration'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Master Configuration'))
      ->setSetting('target_type', 'master_configuration')
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_label',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => -3,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The timestamp that the unit_configuration was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The timestamp that the unit_configuration was last changed.'));

    return $fields;
  }

}
